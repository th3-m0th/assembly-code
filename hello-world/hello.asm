section .data
	text db "Hello, World",10 ; db stands for "define bytes"
	;text is the name of the variable.  each char is a byte and the "10" is a newline character (\n)

section .text
	global _start

_start:
	mov rax, 1
	mov rdi, 1
	mov rsi, text
	mov rdx, 14
	syscall

	mov rax, 60
	mov rdi, 0
	syscall
